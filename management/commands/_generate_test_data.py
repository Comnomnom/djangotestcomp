from djangotest.campaigns.models import *

def gener_data(campaigns, products, competitors):

    for ind1 in range(campaigns):

        camp = Campaign(name="Campaign" + str(ind1), type=1, domain="asdasd")
        camp.save()
        #cycle for competitors
        for ind2 in range(competitors * ind1, competitors * (ind1 + 1)):
            comp = Competitor(campaign=camp, name='Competitor' + str(ind2), domain='sadsa')
            comp.save()
        #cycle for product
        for ind3 in range(products * ind1, products * (ind1 + 1)):
            #select price zero, with numbers after coma and without
            rand_numb = ind1 + ind3**2
            if rand_numb % 3 == 0:
                high_level = (campaigns * products * (ind3 + 1)**2) % 1001
                low_level = ((competitors * products * (ind3 + 1)**3) % 100) / 100.0
                prod_price = high_level + low_level
            elif rand_numb % 3 == 1:
                prod_price = (campaigns * products * (ind3 + 1)**2) % 1001
            else:
                prod_price = 0
            prod = Product(campaign=camp, url="url"+str(ind3), title="Product" + str(ind3), price=prod_price)
            prod.save()

        comp_models = Competitor.objects.filter(campaign=camp)
        prod_models = Product.objects.filter(campaign=camp)
        comp_id_list = list(comp_models.values_list('id', flat=True))
        prod_id_list = list(prod_models.values_list('id', flat=True))
        copy_comp_id_list = list(comp_id_list)
        copy_prod_id_list = list(prod_id_list)
        shorter_list = 0
        if len(prod_id_list) > len(comp_id_list):
            shorter_list = len(comp_id_list)
        else:
            shorter_list = len(prod_id_list)
        # gener mathching so all product and competitor have at least 1 matching
        for i in range(shorter_list):

            rand_num1 = ((i+1) ** 3 * shorter_list) % len(prod_id_list)
            pointer_prod = prod_id_list.pop(rand_num1)
            prod = Product.objects.filter(id=pointer_prod).first()

            rand_num2 = ((i+1)**2 * competitors) % len(comp_id_list)
            pointer_comp = comp_id_list.pop(rand_num2)
            comp = Competitor.objects.filter(id=pointer_comp).first()

            match = Matching(product=prod, competitor=comp, url="Urlshigidin", price=prod.price)
            match.save()
        #fill random others
        if len(prod_id_list) > 0:
            for prod_id in prod_id_list:
                prod = Product.objects.filter(id=prod_id).first()
                number = ((prod_id + 1 + ind1)**2) % len(copy_comp_id_list)
                comp_id = copy_comp_id_list[number]
                comp = Competitor.objects.filter(id=comp_id).first()
                match = Matching(product=prod, competitor=comp, url="Urlshigidin", price=prod.price)
                match.save()
        else:
            for comp_id in comp_id_list:
                comp = Competitor.objects.filter(id=comp_id).first()
                number = ((comp_id + 1 + ind1) ** 2) % len(copy_prod_id_list)
                prod_id = copy_prod_id_list[number]
                prod = Product.objects.filter(id=prod_id).first()
                match = Matching(product=prod, competitor=comp, url="Urlshigidin", price=prod.price)
                match.save()



