from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from djangotest.campaigns.models import *


class Command(BaseCommand):

    help = "delete existing test data"


    def add_arguments(self, parser):
        parser.add_argument('opt1', nargs='+', type=int)

    def handle(self, *args, **kwargs):

        Campaign.objects.all().delete()
        Product.objects.all().delete()
        Competitor.objects.all().delete()
        Matching.objects.all().delete()