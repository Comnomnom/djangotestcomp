from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from djangotest.management.commands._generate_test_data import gener_data


class Command(BaseCommand):

    help = "My test command"

    option_list = BaseCommand.option_list + (
        make_option('--campaigns',
                    action='store',
                    help='print number of campaigns'),
        make_option('--products',
                    action='store',
                    help='print number of products'),
        make_option('--competitors',
                    action='store',
                    help='print number of competitors'),
    )

    def add_arguments(self, parser):
        parser.add_argument('opt1', nargs='+', type=int)

    def handle(self, *args, **kwargs):

        campaigns, products, competitors = 10, 5, 10

        if kwargs["campaigns"] != None:
            campaigns = int(kwargs["campaigns"])
        if kwargs["products"] != None:
            products =  int(kwargs["products"])
        if kwargs["competitors"] != None:
            competitors = int(kwargs["competitors"])


        gener_data(campaigns, products, competitors)



