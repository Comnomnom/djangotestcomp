from django.conf.urls import url, include

from tastypie.serializers import Serializer
from tastypie.resources import Resource, ModelResource
from tastypie.resources import DeclarativeMetaclass, ModelDeclarativeMetaclass
from tastypie import fields
from tastypie import http
from tastypie.bundle import Bundle
from tastypie.utils import trailing_slash
from tastypie.exceptions import ImmediateHttpResponse

import csv
import collections
import StringIO
import io
import openpyxl as pyxl


from djangotest.campaigns.models import *


def convert_dict_utf8(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(convert_dict_utf8, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convert_dict_utf8, data))
    else:
        return data

class ExtendedSerializer(Serializer):
    formats = Serializer.formats + ['csv'] + ['excel']

    content_types = dict(
        Serializer.content_types.items() +
        [('csv', 'text/csv')] + [('excel', 'spreadsheetml/xlsx')])

    def to_csv(self, data, options=None):
        options = options or {}
        data = self.to_simple(data, options)
        raw_data = StringIO.StringIO()
        if data['objects']:
            fields = data['objects'][0].keys()
            writer = csv.DictWriter(raw_data, fields,
                                    dialect="excel",
                                    extrasaction='ignore')
            writer.writeheader()  # In Python 2.7: `writer.writeheader()`
            for item in data['objects']:
                writer.writerow(item)

        return raw_data.getvalue()

    def from_csv(self, content):
        raw_data = io.BytesIO(content)

        data = []
        # Untested, so this might not work exactly right.
        for item in csv.DictReader(raw_data):
            data.append(item)
        return data

    def to_excel(self, data, options=None):

        data = self.to_simple(data, options)
        data = convert_dict_utf8(data)
        wb = pyxl.Workbook()

        ws = wb['Sheet']

        col_head = 1
        for key in data['objects'][0].keys():
            ws.cell(row=1, column=col_head, value=key)
            col_head += 1

        row = 2

        for item in data['objects']:
            col = 1
            for value in item.values():
                ws.cell(row=row, column=col, value=str(value))
                col += 1
            row += 1

        return pyxl.writer.excel.save_virtual_workbook(wb)

    def from_excel(self, content):
        raw_data = io.BytesIO(content)

        wb = pyxl.load_workbook(raw_data)
        #create Sheet, but here i have empty Sheet but full with data Sheet1, Magic...
        ws = wb['Sheet']
        data = []
        keys_list = [str(cell.value) for cell in ws[1]]

        for row in ws.iter_rows(min_row=2):
            dict_row = {}
            for key, cell in zip(keys_list, row):
                dict_row[key] = str(cell.value)
            data.append(dict_row)
        return data

class CampaignResource(ModelResource):
    settings = fields.ToOneField(
        'djangotest.campaigns.api.CampaignSettingsResource', lambda b: b.obj)

    class Meta:
        resource_name = 'campaigns'
        queryset = Campaign.objects.all()
        fields = ('id', 'name', 'type', 'domain', 'is_active')
        serializer = ExtendedSerializer(formats=['json', 'csv', 'excel'])

    _sub_resources = None

    def __init__(self, *args, **kwargs):
        super(CampaignResource, self).__init__(*args, **kwargs)
        self._sub_resources = []

    def prepend_urls(self):
        return [
            url(r'^%s/(?P<campaign_id>\w+)/' % self._meta.resource_name,
                include(resource.urls))
            for resource in self._sub_resources
        ]

    def register_sub_resource(self, resource):
        self._sub_resources.append(resource)
        return resource

    def dispatch(self, request_type, request, **kwargs):
        return super(CampaignResource, self).dispatch(
            request_type, request, **kwargs)


class CampaignSubResourceDeclarativeMetaclass(ModelDeclarativeMetaclass):
    def __new__(cls, name, bases, attrs):
        meta = attrs.get('Meta')
        if meta is not None:
            if getattr(meta, 'one_to_one_resource', None) is None:
                setattr(meta, 'one_to_one_resource', False)
            if getattr(meta, 'campaign_keyword', None) is None:
                setattr(meta, 'campaign_keyword', 'campaign_id')

        new_class = super(CampaignSubResourceDeclarativeMetaclass, cls).__new__(
            cls, name, bases, attrs)

        return new_class


class CampaignSubResource(ModelResource):
    __metaclass__ = CampaignSubResourceDeclarativeMetaclass

    def _set_campaign(self, request, **kwargs):
        if not request.user.is_authenticated():
            return request

        try:
            campaign_id = int(kwargs['campaign_id'])
        except (KeyError, ValueError):
            raise ImmediateHttpResponse(response=http.HttpNotFound())

        try:
            request.campaign = Campaign.objects.get(pk=campaign_id)
        except Campaign.DoesNotExist:
            raise ImmediateHttpResponse(response=http.HttpNotFound())

        return request

    def get_object_list(self, request):
        objects = super(CampaignSubResource, self).get_object_list(request)
        if not hasattr(objects, 'model'):
            return objects

        campaign = request.campaign

        if objects.model is Matching:
            objects = objects.filter(competitor__campaign=campaign)
        elif 'campaign' in objects.model._meta.get_all_field_names():
            objects = objects.filter(campaign=campaign)

        return objects

    def dispatch(self, request_type, request, **kwargs):

        self.is_authenticated(request)

        request = self._set_campaign(request, **kwargs)

        if self._meta.campaign_keyword != 'campaign_id':
            kwargs[self._meta.campaign_keyword] = kwargs.pop('campaign_id')

        return super(CampaignSubResource, self).dispatch(
            request_type, request, **kwargs)

    def base_urls(self):
        if self._meta.one_to_one_resource:
            urls =  [
                url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_schema'), name="api_get_schema"),
                url(r"^(?P<resource_name>%s)%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
            ]
        else:
            urls = super(CampaignSubResource, self).base_urls()

        return urls

    def resource_uri_kwargs(self, bundle_or_obj=None):

        if isinstance(bundle_or_obj, Bundle) and bundle_or_obj.obj is None:
            bundle_or_obj_to_pass = None
        else:
            bundle_or_obj_to_pass = bundle_or_obj

        kwargs = super(CampaignSubResource, self).resource_uri_kwargs(bundle_or_obj_to_pass)
        
        if isinstance(bundle_or_obj, Bundle):
            bundle = bundle_or_obj
            obj = bundle_or_obj.obj
        else:
            bundle = None
            obj = bundle_or_obj

        if obj is not None and hasattr(obj, 'campaign_id'):
            kwargs['campaign_id'] = obj.campaign_id
        elif isinstance(obj, Campaign):
            kwargs['campaign_id'] = obj.pk
        elif bundle is not None:
            kwargs['campaign_id'] = bundle.request.campaign.pk

        if self._meta.one_to_one_resource:
            del kwargs[self._meta.detail_uri_name]
        return kwargs


class CampaignSettingsResource(CampaignSubResource):
    class Meta:
        resource_name = 'settings'
        one_to_one_resource = True
        campaign_keyword = 'pk'
        queryset = Campaign.objects.all()
        fields = ('is_scanning_enabled', 'is_url_rewriting_enabled')


class ProductResource(ModelResource):
    price = fields.FloatField(
        attribute='price', null=True, blank=True, use_in='detail')
    class Meta:
        resource_name = 'products'
        queryset = Product.objects.all()
        fields = ["title", "url", "price"]
        serializer = ExtendedSerializer(formats=['json', 'csv', 'excel'])
        limit = 5


class CompetitorResource(CampaignSubResource):
    class Meta:
        resource_name = 'competitors'
        queryset = Competitor.objects.all()


class MatchingResource(ModelResource):

    class Meta:
        resource_name = 'matchings'
        queryset = Matching.objects.all()
        fields = ("price", "url", 'id')
        serializer = ExtendedSerializer(formats=['json', 'csv', 'excel'])
        limit = 5


    def dehydrate(self, bundle):
        matching = Matching.objects.get(id=bundle.data["id"])
        competitor = Competitor.objects.filter(id=matching.competitor_id).first()
        product = Product.objects.filter(id=matching.product_id).first()
        bundle.data["name"] = competitor.name
        bundle.data["title"] = product.title
        bundle.data['product_url'] = product.url
        del bundle.data["id"]
        convert_dict_utf8(bundle.data)
        return bundle

