from django.shortcuts import render
from django.http.response import HttpResponse
from django.shortcuts import render_to_response
from djangotest.campaigns.models import *
from djangotest.testutils import RestApiClient

# Create your views here.
class Matching_data():
    def __init__(self, competitor, product, price):
        self.competitor = competitor
        self.product = product
        self.price = price

def form_match_data(campaign_name):
    matching_li = []
    camp = Campaign.objects.filter(name=campaign_name)
    competitors = Competitor.objects.filter(campaign=camp)
    for comp in competitors:
        matchings = Matching.objects.filter(competitor=comp)
        for match in matchings:
            product = Product.objects.filter(id=match.product_id).first()
            matching_li.append(Matching_data(comp.name, product.title, product.price))


    return {"matchings": matching_li}

def display_campaign(request, campaign_name):
    campaign = Campaign.objects.filter(name=campaign_name).first()
    dict = form_match_data(campaign_name)
    return render_to_response('matching_template.html', dict)





